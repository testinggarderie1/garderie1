const Datastore = require('@google-cloud/datastore');

const datastore = new Datastore({
  projectId: 'manager-218900',
});

function Family(familyInfos){
  return {
    key : datastore.key(['Family' , familyInfos.id]),
    data: {}
  };
}

function Child(childInfos){

  return {
    key : datastore.key(['Family' , childInfos.familyId, 'Child', childInfos.id]),
    data: {
      lastName : childInfos.lastName,
      firstName : childInfos.firstName,
      sex : childInfos.sex,
      birthDate : new Date(childInfos.birthDate),
      startDate : new Date(childInfos.startDate),
      endDate : new Date(childInfos.endDate)
    }
  };
}

function Parent(parentInfos){

  return {
    key : datastore.key(['Family', parentInfos.familyId, 'Parent', parentInfos.id]),
    data: {
      lastName : parentInfos.lastName,
      firstName : parentInfos.firstName,
      sex : parentInfos.sex,
      birthDate : new Date(parentInfos.birthDate),
      phone1 : parentInfos.phone1,
      phone2 : parentInfos.phone2,
      mail1 : parentInfos.mail1,
      mail2 : parentInfos.mail2
    }
  };
}

function PaymentSheet(paymentSheetInfos){

  return {
    key : datastore.key(['PaymentSheet', paymentSheetInfos.id]),
    data: {
      year : parseInt(paymentSheetInfos.year),
      month : parseInt(paymentSheetInfos.month),
    }
  };
}

function FamilyBill(familyBillInfos){
  return {
    key : datastore.key(['PaymentSheet', familyBillInfos.paymentSheetId, 'FamilyBill', familyBillInfos.id]),
    data: {
      familyId : familyBillInfos.familyId
    }
  };
}

function ChildBill(childBillInfos){

  return {
    key : datastore.key(['PaymentSheet', childBillInfos.paymentSheetId, 'FamilyBill',
      childBillInfos.familyBillId, 'ChildBill', childBillInfos.id]),
    data: {
      childId : childBillInfos.childId,
      amount: datastore.double(childBillInfos.amount),
    }
  };
}

function InteracPayment(interacPaymentInfos){
  return {
    key : datastore.key(['PaymentSheet', interacPaymentInfos.paymentSheetId, 'FamilyBill',
      interacPaymentInfos.familyBillId, 'ChildBill', interacPaymentInfos.childBillId, 'InteracPayment',
      interacPaymentInfos.id]),
    data: [
  {name: 'interacMessageId', value : interacPaymentInfos.interacMessageId},
  {name : 'moneySent', value : interacPaymentInfos.moneySent},
  {name: 'sender', value: interacPaymentInfos.sender},
  {name : 'dateOfPayment', value: interacPaymentInfos.dateOfPayment},
  {name : 'encodedMessageContent', value : interacPaymentInfos.encodedMessageContent, excludeFromIndexes: true}]
  };
}

function AdvancedInteracPayment(interacPaymentInfos){
  return {
    key : datastore.key([ 'AdvancedInteracPayment', interacPaymentInfos.id]),
    data: [
      {name: 'interacMessageId', value : interacPaymentInfos.interacMessageId},
      {name : 'moneySent', value : interacPaymentInfos.moneySent},
      {name: 'sender', value: interacPaymentInfos.sender},
      {name : 'dateOfPayment', value: interacPaymentInfos.dateOfPayment},
      {name : 'familyId', value: interacPaymentInfos.familyId},
      {name : 'encodedMessageContent', value : interacPaymentInfos.encodedMessageContent, excludeFromIndexes: true}]
  };
}

function generateEntity(payload){
  switch (payload.kind) {
  case 'PaymentSheet':
    return PaymentSheet(payload);
  case 'FamilyBill':
    return FamilyBill(payload);
  case 'ChildBill':
    return ChildBill(payload);
  case 'InteracPayment':
    return InteracPayment(payload);
  case 'AdvancedInteracPayment':
    return AdvancedInteracPayment(payload);
  case 'Parent':
    return Parent(payload);
  case 'Child':
    return Child(payload);
  case 'Family':
    return Family(payload);
  default:
    throw new Error('Unknown kind selected');
  }
}

module.exports = { generateEntity };