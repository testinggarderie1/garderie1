const validator = require('validator');

exports.verifyPayload = (infos)=>{
  switch (infos.kind) {
    case 'PaymentSheet':
      return verifyPaymentSheet(infos);
    case 'FamilyBill':
      return verifyFamilyBill(infos);
    case 'ChildBill':
      return verifyChildBill(infos);
    case 'InteracPayment':
      return verifyInteracPayment(infos);
    case 'AdvancedInteracPayment':
      return verifyAdvancedInteracPayment(infos);
    case 'Parent':
      return verifyParent(infos);
    case 'Child':
      return verifyChild(infos);
    case 'Family':
      return verifyFamily(infos);
    default:
      throw new Error('Unknown kind passed for verification');
  }
};

function verifyChild(infos){
  let errors = [];
  const missingAttributesToVerify = ['lastName', 'firstName', 'sex', 'birthDate', 'startDate', 'endDate', 'familyId'];
  const datesToVerify = ['birthDate', 'startDate', 'endDate'];

  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifySex(infos));
  errors.push(verifyDates(infos, datesToVerify));

  throwError(errors)
}

function verifyParent(infos){
  let errors = [];
  const missingAttributesToVerify = ['lastName', 'firstName', 'sex', 'birthDate',
    'phone1', 'mail1', 'familyId'];
  const datesToVerify = ['birthDate'];
  const phoneNumbersToVerify = ['phone1'];
  const emailsToVerify = ['mail1'];

  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifySex(infos));
  errors.push(verifyDates(infos, datesToVerify));
  errors.push(verifyPhones(infos, phoneNumbersToVerify));
  errors.push(verifyEmails(infos, emailsToVerify));

  throwError(errors)
}

function verifyFamily(infos){
  let errors = [];
  const missingAttributesToVerify = ['id'];
  const ids = [['familyId', 'id']];

  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifyId(ids, infos));

  throwError(errors)
}

function verifyChildBill(infos) {
  let errors = [];
  const missingAttributesToVerify = ['paymentSheetId', 'childId', 'familyBillId', 'amount'];
  const ids = [['childId', 'childId'], ['familyBillId', 'familyBillId'], ['paymentSheetId', 'paymentSheetId']];
  const floats = ['amount'];

  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifyId(ids, infos));
  errors.push(verifyFloat(infos, floats));

  throwError(errors)
}

function verifyFamilyBill(infos){
  let errors = [];
  const missingAttributesToVerify = ['familyId', 'paymentSheetId'];
  const ids = [['familyId', 'familyId'], ['paymentSheetId', 'paymentSheetId']];

  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifyId(ids, infos));

  throwError(errors)
}

function verifyAdvancedInteracPayment(infos){
  let errors = [];
  const missingAttributesToVerify = ['interacMessageId', 'moneySent', 'sender', 'encodedMessageContent', 'familyId'];
  let ids = [['familyId', 'familyId']];
  const floats = ['moneySent'];
  const datesToVerify = ['dateOfPayment'];

  errors.push(verifyFloat(infos, floats));
  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifyId(ids, infos));
  errors.push(verifyDates(infos, datesToVerify));

  throwError(errors)
}

function verifyInteracPayment(infos){
  let errors = [];
  const missingAttributesToVerify = ['interacMessageId', 'moneySent', 'sender', 'encodedMessageContent'];
  const ids = [['paymentSheetId', 'paymentSheetId'], ['familyBillId', 'familyBillId'], ['childBillId', 'childBillId']];

  const floats = ['moneySent'];
  const datesToVerify = ['dateOfPayment'];

  errors.push(verifyFloat(infos, floats));
  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifyId(ids, infos));
  errors.push(verifyDates(infos, datesToVerify));

  throwError(errors)
}

function verifyPaymentSheet(infos){
  let errors = [];
  const missingAttributesToVerify = ['year', 'month'];
  const integers =['month', 'year'];

  errors.push(verifyMissing(infos, missingAttributesToVerify));
  errors.push(verifyInteger(infos, integers));

  throwError(errors)
}

function verifyId(ids, infos){
  let errorMessage = "" ;
  let throwError = false;
  const familyIdRegEx = /genie\d{0,4}/;
  const personIdRegEx = /[A-Z][a-z]* [A-Z][a-z]* *\d*/;
  const paymentSheetRegEx = /PaymentSheet-(1[0-1]|[1-9])-\d{4}/;
  const familyBillRegEx = /FamilyBill-genie\d{0,4}/;
  const childBillRegEx = /ChildBill-[A-Z][a-z]* [A-Z][a-z]* *\d*/;
  const interacPaymentRegEx = /InteracPayment-[A-Z][a-z]* [A-Z][a-z]* *\d*/;
  for(const id of ids){
    if(infos[id[1]]){
      switch (id[0]) {
        case 'familyId':
          if(!familyIdRegEx.test(infos[id[1]])){
            throwError = true;
            errorMessage+=`${id[1]} : ${infos[id[1]]} is wrong `;
          }
          break;
        case 'childId':
          if(!personIdRegEx.test(infos[id[1]])){
            throwError = true;
            errorMessage+=`${id[1]} : ${infos[id[1]]} is wrong `;
          }
          break;
        case 'parentId':
          if(!personIdRegEx.test(infos[id[1]])){
            throwError = true;
            errorMessage+=`${id[1]} : ${infos[id[1]]} is wrong `;
          }
          break;
        case 'paymentSheetId':
          if(!paymentSheetRegEx.test(infos[id[1]])){
            throwError = true;
            errorMessage+=`${id[1]} : ${infos[id[1]]} is wrong `;
          }
          break;
        case 'familyBillId':
          if(!familyBillRegEx.test(infos[id[1]])){
            throwError = true;
            errorMessage+=`${id[1]} : ${infos[id[1]]} is wrong `;
          }
          break;
        case 'childBillId':
          if(!childBillRegEx.test(infos[id[1]])){
            errorMessage+=`${id[1]} : ${infos[id[1]]} is wrong `;
          }
          break;
        case 'interacPaymentId':
          if(!interacPaymentRegEx.test(infos[id[1]])){
            throwError = true;
            errorMessage+=`${id[1]} : ${infos[id[1]]} is wrong `;
          }
          break;
        default:
          throw new Error('Unknown id type selected');
      }
    }
    if(!infos[id[1]]){
      throwError = true;
      errorMessage+=`${id[0]} is undefined `;
    }
  }
  return [throwError, errorMessage];
}

function verifyPhones(infos, phones){
  let raiseError = false;
  let errorMessage = `phone numbers `;
  for(const phone of phones){
    if(!validator.isMobilePhone(infos[phone], 'en-CA')){
      raiseError = true;
      errorMessage+=`${infos[phone]} `;
    }
  }
  errorMessage+=`are not valid Canadian numbers`;

  return [raiseError, errorMessage];
}

function verifyEmails(infos, emails){
  let raiseError = false;
  let errorMessage = `emails `;
  for(const email of emails){
    if(!validator.isEmail(infos[email])){
      raiseError = true;
      errorMessage+=`${infos[email]} `;
    }
  }
  errorMessage+=`are not valid`;

  return [raiseError, errorMessage];
}

function verifyDates(infos, datesToVerify){
  let raiseError = false;
  let errorMessage = `dates `;
  for(const date of datesToVerify){
    const dateToVerify = new Date(infos[date]);
    if(!validator.isRFC3339(dateToVerify.toISOString())){
      raiseError = true;
      errorMessage += ` ${date} : ${infos[date]} `;
    }else{
      infos[date] = dateToVerify;
    }
  }
  errorMessage += `are not RFC3339 format`;

  return [raiseError, errorMessage];
}

function verifySex(infos){
  const possibleSex = ['M', 'F'];
  let errorMessage = `${infos.sex} is not a possible sex`;
  let raiseError = false;
  if(!possibleSex.includes(infos.sex)){
    raiseError = true;
  }
  return [raiseError, errorMessage];
}

function verifyMissing(infos, attributesToVerify){
  let raiseError = false;
  let missingAttributesErrorMessage = `attributes `;
  for(const attribute of attributesToVerify){
    if(!infos[attribute]){
      raiseError = true;
      missingAttributesErrorMessage += `${attribute} `;
    }
  }
  missingAttributesErrorMessage+='are missing';

  return [raiseError, missingAttributesErrorMessage];
}

function verifyFloat(infos, integers){
  let raiseError = false;
  let errorMessage = `numbers `;
  for(const integer of integers){
    const formatedNum = parseFloat(infos[integer]);
    if(!formatedNum){
      raiseError = true;
      errorMessage+=`${infos[integer]} `;
    }else{
      infos[integer] = formatedNum;
    }
  }
  errorMessage+=`are not floats`;

  return [raiseError, errorMessage];
}

function verifyInteger(infos, integers){
  let raiseError = false;
  let errorMessage = `numbers `;
  for(const integer of integers){
    const formatedNum = parseInt(infos[integer]);
    if(!formatedNum && formatedNum!==0){
      raiseError = true;
      errorMessage+=`${infos[integer]} `;
    }else{
      infos[integer] = formatedNum;
    }
  }
  errorMessage+=`are not integers`;

  return [raiseError, errorMessage];
}

function throwError(errors){
  let throwErr = false;
  let finalMessageError = "";
  for(const error of errors){
    throwErr = error[0] === true ? true : throwErr;
    finalMessageError += error[0] === true ? error[1] + '\n' : "";
  }
  if(throwErr){
    throw new Error(finalMessageError);
  }
}













