const EntityGenerator = require('./EntityGenerator');
const EntityVerifier = require('./EntityVerifier');
const IDGenerator = require('./IDGenerator');

const DatastoreDB =  require('./DatastoreBD');

exports.processPayload = async (payload) => {
  return new Promise(async (resolve, reject) => {
    try{
      const entity = await preprocessPayloadDB(payload);
      resolve(entity);
    }catch (err) {
      console.error(err);
      reject(err);
    }
  });
};

async function preprocessPayloadDB(payload){
  EntityVerifier.verifyPayload(payload);
  const payloadWithId = await IDGenerator.generateId(payload);
  return EntityGenerator.generateEntity(payloadWithId);
}

exports.addEntity  = async (payload) => {
  return new Promise(async (resolve, reject) => {
    try{
      const entity =  await preprocessPayloadDB(payload);
      await DatastoreDB.save(entity);
      resolve(entity);
    }catch (err) {
      console.error(err);
      reject(err);
    }
  });
};

exports.deleteEntity =  async (payload) => {
  return new Promise(async (resolve, reject) => {
    await DatastoreDB.del(payload)
    .then(()=>{resolve(payload);})
    .catch((err)=>{reject(err)});
  });
};
