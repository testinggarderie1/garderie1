const DataStoreDB = require('./DatastoreBD');
const Datastore = require('@google-cloud/datastore');

async function idGeneratorSelector(payload){
  switch (payload.kind) {
    case 'PaymentSheet':
      return generatepaymentSheetId(payload);
    case 'FamilyBill':
      return generateFamilyBillId(payload);
    case 'ChildBill':
      return generateChildBillId(payload);
    case 'InteracPayment':
      return generateInteracPaymentId(payload);
    case 'AdvancedInteracPayment':
      return generateAdvancedInteracPaymentId(payload);
    case 'Child' :
      return generateChildId(payload);
    case 'Parent' :
      return generateParentId(payload);
    case 'Family' :
      return generateAFamilyId(payload);
    default:
      throw new Error('Unknown kind selected');
  }
}

async function generateId(payload){
  payload.id = await idGeneratorSelector(payload);
  return payload;
}

async function generateChildId(childInfos){
  return `${childInfos.lastName} ${childInfos.firstName}`;
}

async function generateParentId(parentInfos){
  return `${parentInfos.lastName} ${parentInfos.firstName}`;
}

function generateFamilyBillId(familyBillInfos){
  return `FamilyBill-${familyBillInfos.familyId}`;
}

function generateChildBillId(childBillInfos){
  return `ChildBill-${childBillInfos.childId}`;
}

function generateInteracPaymentId(interacPaymentInfos){
  return `InteracPayment-${interacPaymentInfos.childBillId}-${interacPaymentInfos.interacMessageId}`;
}

function generateAdvancedInteracPaymentId(interacPaymentInfos){
  return `AdvancedInteracPayment-${interacPaymentInfos.interacMessageId}`;
}

function generatepaymentSheetId(paymentSheetInfos){
  return `PaymentSheet-${paymentSheetInfos.month}-${paymentSheetInfos.year}`
}

function suggestAFamilyID(){
  const min = 0;
  const max = 1000;
  return `genie${Math.floor(Math.random() * (max - min) + min)}`;
}

async function generateAFamilyId(){
  const familyIds = await getFamilyIds();
  if(1000 <= familyIds.length){
    throw new Error(`no more Family Ids available`);
  }
  let suggestedID = suggestAFamilyID();
  while(familyIds.includes(suggestedID)){
    suggestedID = suggestAFamilyID();
  }
  return suggestedID;
}

async function getFamilyIds(){
  let familyIds = [];
  const families = await DataStoreDB.search([], 'Family');
  for(const family of families){
    familyIds.push(family[Datastore.KEY].name);
  }
  return familyIds;
}

module.exports = {generateId};