const Datastore = require('@google-cloud/datastore');

const datastore = new Datastore({
  projectId: 'manager-218900',
});

async function save(entity){

  await datastore
  .save(entity)
  .then(() => {
    console.log(`Saved ${entity.key.kind}: ${entity.key.name}`);
  });
  return entity.key.name;
}

async function search(filters, kind){

  const query = datastore
  .createQuery(kind);

  for(const filter of filters){
    if(filter[0] === 'hasAncestor'){
      query.hasAncestor(filter[1]);
      continue;
    }
    query.filter(filter[0], '=', filter[1]);
  }

  const [entities] = await datastore.runQuery(query);
  console.log(`${kind}s:`);
  entities.forEach(entity => console.log(entity));

  return entities;
}

async function get(entityKey){
  const [entity] = await datastore.get(entityKey);
  if(entity){
    console.log(`retreived : ${entity[datastore.KEY].kind} - ${entity[datastore.KEY].name}`);
    return entity;
  }
  return null;
}

async function del(key){
  const entityKey = datastore.key(key);
  await datastore.delete(entityKey).then(() => {
    console.log(`deleted : ${entityKey.kind} - ${entityKey.name}`);
  });
}

async function update(entity){
  await datastore.update(entity).then(() => {
    console.log(`deleted : ${entity.key.kind} - ${entity.key.name}`);
  });
}

module.exports = {save, get, del, update, search};