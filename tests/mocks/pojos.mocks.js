
const FamilyInfosTest = {kind : 'Family', id:'genie1234'};

const Child1InfosTest = {kind : 'Child', lastName: 'Mahjoubi', firstName: 'Taha', sex: 'M',
  birthDate: '2016-07-30T09:30:16.768-04:00', startDate: '2017-12-22T09:30:16.768-04:00',
  endDate: '2023-12-10T09:30:16.768-04:00', familyId: 'genie234'};

const Child2InfosTest = {kind : 'Child', lastName: 'Mahjoubi', firstName: 'Hakim', sex: 'M',
  birthDate: '2019-06-03T09:30:16.768-04:00', startDate: '2019-12-15T09:30:16.768-04:00',
  endDate: '2024-12-06T09:30:16.768-04:00', familyId: 'genie234'};

const Child3InfosTest = {kind : 'Child', lastName: 'Mahjoubi', firstName: 'Ahmed', sex: 'M',
  birthDate: '2019-05-03T09:30:16.768-04:00', startDate: '2018-12-01T09:30:16.768-04:00',
  endDate: '2025-12-01T09:30:16.768-04:00', familyId: 'genie234'};


const Parent1InfosTest = {kind : 'Parent', lastName: 'Mahjoubi', firstName: 'Sarra', sex: 'F',
  birthDate: '1980-02-23T09:30:16.768-04:00', phone1:'4182345432', mail1:'sarra@gmail.com', familyId: 'genie234'};

const Parent2InfosTest = {kind : 'Parent', lastName: 'Bejaoui', firstName: 'Slah', sex: 'M',
  birthDate: '1970-02-23T09:30:16.768-04:00', phone1:'4185646262', mail1:'slah@gmail.com', familyId: 'genie234'};

const PaymentSheetInfosTest = {kind : 'PaymentSheet', year: '2019', month: '5'};
const FamilyBillInfosTest = {kind : 'FamilyBill', familyId: 'genie234', paymentSheetId: 'PaymentSheet-5-2019'};

const Child1BillInfosTest = {kind : 'ChildBill', childId: 'Mahjoubi Taha', paymentSheetId: 'PaymentSheet-5-2019',
  familyBillId: 'FamilyBill-genie234', amount : 500};
const Child2BillInfosTest = {kind : 'ChildBill', childId: 'Mahjoubi Hakim', paymentSheetId: 'PaymentSheet-5-2019',
  familyBillId: 'FamilyBill-genie234', amount : 600};

const InteracPayment1InfosTest = {kind : 'InteracPayment', interacMessageId: '1234', moneySent: '100', sender: 'Sarra',
  dateOfPayment: '2019-03-23T09:30:16.768-04:00', encodedMessageContent: 'hjdjhfjffhd', paymentSheetId: 'PaymentSheet-5-2019',
  familyBillId: 'FamilyBill-genie234', childBillId: 'ChildBill-Mahjoubi Taha'};
const InteracPayment2InfosTest = {kind : 'InteracPayment', interacMessageId: '2345', moneySent: '200', sender: 'Slah',
  dateOfPayment: '2019-03-23T09:30:16.768-04:00', encodedMessageContent: 'hjdjhfjffhd', paymentSheetId: 'PaymentSheet-5-2019',
  familyBillId: 'FamilyBill-genie234', childBillId: 'ChildBill-Mahjoubi Taha'};
const InteracPayment3InfosTest = {kind : 'InteracPayment', interacMessageId: '6786', moneySent: '300', sender: 'Sarra',
  dateOfPayment: '2019-03-23T09:30:16.768-04:00', encodedMessageContent: 'hjdjhfjffhd', paymentSheetId: 'PaymentSheet-5-2019',
  familyBillId: 'FamilyBill-genie234', childBillId: 'ChildBill-Mahjoubi Hakim'};
const InteracPayment4InfosTest = {kind : 'InteracPayment', interacMessageId: '8765', moneySent: '400', sender: 'Slah',
  dateOfPayment: '2019-03-23T09:30:16.768-04:00', encodedMessageContent: 'hjdjhfjffhd', paymentSheetId: 'PaymentSheet-5-2019',
  familyBillId: 'FamilyBill-genie234', childBillId: 'ChildBill-Mahjoubi Hakim'};

const AvdancedInteracPayment1InfosTest = {kind : 'AdvancedInteracPayment', interacMessageId: '8765', moneySent: '400', sender: 'Slah',
  dateOfPayment: '2019-03-23T09:30:16.768-04:00', encodedMessageContent: 'hjdjhfjffhd', familyId: 'genie234'};


const infos = {
  child1 : Child1InfosTest,
  child2 : Child2InfosTest,
  child3 : Child3InfosTest,
  parent1 : Parent1InfosTest,
  parent2 : Parent2InfosTest,
  family : FamilyInfosTest,
  paymentSheet : PaymentSheetInfosTest,
  familyBill : FamilyBillInfosTest,
  child1Bill : Child1BillInfosTest,
  child2Bill : Child2BillInfosTest,
  interacPayment1 : InteracPayment1InfosTest,
  interacPayment2 : InteracPayment2InfosTest,
  interacPayment3 : InteracPayment3InfosTest,
  interacPayment4 : InteracPayment4InfosTest,
  advancedInteracPayment1 : AvdancedInteracPayment1InfosTest
};

module.exports = {infos};

